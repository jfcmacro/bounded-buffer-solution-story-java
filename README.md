---

# Bounded Buffer Solution Story

**Estimated reading times**: Unknown

## Story Outline

Concurrency is one of the most critical issues in programming today. A lot of programmers avoid that subject and sometimes ignore it completely.

This story goes about a classic concurrent problem: Bounded-Buffer. This problem was proposed and solved by Edge W. Dijkstra; this shows how two processes, producer and consumer, communicate using a limited-size buffer. We start to offer a first approach where the concurrent solution is avoided, but this shows us the relevant part that we have to take into account when we manage concurrent problems, in particular, identify which ones are the parts of the *critical section*. Then we move to the first solution, which uses low-level primitives found in Java Programming Languages: `synchronized` methods (or `synchronized` blocks), with `wait` and notifies ways that mimic the behavior of condition variables.

Next, we show another solution to the problem but use *Semaphores* as a mechanism to manage concurrency. A Semaphore is an integer counter that increments (`release`) and decrements (`acquire`) in an atomic way. When a decrement value is zero, the process or thread invokes that operation is suspended until another process or thread increments the counter.

Finally, we show a final solution using locks and conditions, which is sometimes ...


