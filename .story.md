---
focus: src/main/java/com/epam/rd/boundedbuffer/BoundedBufferLockImpl.java
---

### Conclusion

* Java is a programming language designed with concurrency in mind.
* Java provides different levels of mechanisms to manage concurrency, since low-level mechanisms are based on `synchronized`methods and `synchronized` blocks, to Locks.
* Semaphores are a mechanism that can be found on many operating systems and programming languages.
* In Java, Locks is more flexible and useful.


